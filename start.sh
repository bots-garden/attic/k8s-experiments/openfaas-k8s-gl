#!/bin/sh
cd 01-docker-registry
vagrant up
cd ..
cd 02-k8s-openfaas
vagrant up
cd ..
cd 04-gitlab
vagrant up
cd ..
cd 05-runners
vagrant up
cd ..