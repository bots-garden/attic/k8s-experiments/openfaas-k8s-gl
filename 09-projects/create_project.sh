#!/bin/sh

source core.sh

# create a user with your handle (k33G) and admin rights + ssh key
token=$OF_GITLAB_USER_TOKEN
gitlab_url=$OF_GITLAB_URL
registry_domain=$OF_DOCKER_REGISTRY_DOMAIN
openfaas_url=$OF_OPENFAAS_URL
# create a bot user on GitLab and admin rights
bot_token=$OF_GITLAB_BOT_TOKEN

project=$( rawurlencode "$1" )
group=$( rawurlencode "$2" )

echo "🚀 > creating $project in group $group"


group_id=$(curl --request GET --header "PRIVATE-TOKEN: $token" "$gitlab_url/api/v4/groups?search=$group" \
--header "Content-Type: application/json" | jq '.[0] | .id')

echo  "🚀 > group id: $group_id"

generate_post_project_data() {
cat <<EOF
  {"name":"$project","path":"$project", "namespace_id":"$group_id"}
EOF
}

curl --request POST --header "PRIVATE-TOKEN: $token" "$gitlab_url/api/v4/projects" \
--header "Content-Type: application/json" \
--data "$(generate_post_project_data)"

# ./create_project.sh plop demo
