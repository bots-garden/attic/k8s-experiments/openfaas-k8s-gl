### DEMO 1

- create `cows` function from the ui

```
faas-cli remove --gateway k8s.test:31112 cows
```

### DEMO 2

```
mkdir hello-prj; cd hello-prj
faas-cli new --lang node --gateway k8s.test:31112 --prefix registry.test:5000 demo
faas-cli build -f ./demo.yml
faas-cli push -f ./demo.yml
curl http://registry.test:5000/v2/_catalog
faas-cli deploy -f ./demo.yml

faas-cli remove --gateway k8s.test:31112 demo
```

### DEMO 3 (shell)


```shell
mkdir cli-prj; cd cli-prj
faas-cli new --lang dockerfile --gateway k8s.test:31112 --prefix registry.test:5000 hello-world
touch ./hello-world/hello.sh; echo '#!/bin/sh' >> ./hello-world/hello.sh; chmod +x ./hello-world/hello.sh
# add a line with echo "👋 hello world 🌍 ... 😃"

#faas-cli build -f ./hello-world.yml
#faas-cli push -f ./hello-world.yml
#curl http://registry.test:5000/v2/_catalog
#faas-cli deploy -f ./hello-world.yml

faas-cli up -f ./hello-world.yml

faas-cli remove --gateway k8s.test:31112 hello-world
```

### DEMO 4 (Express Template - my own template)

```shell
mkdir webapp-prj; cd webapp-prj
faas template pull https://gitlab.com/openfaas-experiments/static-vue-express-template
#faas template pull http://gitlab.test/attic/static-vue-express-template
faas-cli new --lang static-vue-express --gateway k8s.test:31112 --prefix registry.test:5000 hello-web
faas-cli up -f ./hello-web.yml

faas-cli remove --gateway k8s.test:31112 hello-web
```

### GitLab Demo

- create a group
- go to the CI/CD settings of the group: get the token and the url to register your local runner
- register the runner

#### DEMO 5 (GitLab CI/CD quickly explained)

> - Create an empty project in GitLab with a README file in you group
> - Create a `.gitlab-ci.yml` file


### DEMO 6 (use the express vue template)

> with the GitLab UI

```yaml
stages:
  - 🚀deploy
  - 🦄deploy

cache:
  key: "$CI_PIPELINE_ID"
  paths:
  # keep all yaml files between stages
    - ./*.yml

# === Building, Pushing, Deploying OpenFaaS function ===
🚧production_deploy:
  stage: 🚀deploy
  tags:
    - openfaas
  only:
    - master  
  environment:
    name: production/$CI_PROJECT_NAME
    url:  $OPENFAAS_URL/function/$CI_PROJECT_NAME
  script: |
    export FUNCTION_NAME=$CI_PROJECT_NAME
    export SUFFIX=""
    export OPENFAAS_URL=$OPENFAAS_URL
    export REGISTRY_DOMAIN=$REGISTRY_DOMAIN

    # substitution of value in the deployment file
    envsubst < web-app.yml > $CI_PROJECT_NAME.$CI_COMMIT_REF_NAME.yml
    cat $CI_PROJECT_NAME.$CI_COMMIT_REF_NAME.yml

    # build, push, deploy the function image
    faas-cli up -f $CI_PROJECT_NAME.$CI_COMMIT_REF_NAME.yml

# === Building OpenFaaS function for preview ===
🚧preview_deploy:
  stage: 🦄deploy
  tags:
    - openfaas
  only:
    - merge_requests
  environment:
    name: preview/$CI_PROJECT_NAME-$CI_COMMIT_REF_NAME
    url:  $OPENFAAS_URL/function/$CI_PROJECT_NAME-$CI_COMMIT_REF_NAME
    on_stop: 🗑stop_preview_function
  script: |
    export FUNCTION_NAME=$CI_PROJECT_NAME
    export SUFFIX="-$CI_COMMIT_REF_NAME" # 👋👋👋
    export OPENFAAS_URL=$OPENFAAS_URL
    export REGISTRY_DOMAIN=$REGISTRY_DOMAIN
    
    # substitution of value in the deployment file
    envsubst < web-app.yml > $CI_PROJECT_NAME.$CI_COMMIT_REF_NAME.yml
    cat $CI_PROJECT_NAME.$CI_COMMIT_REF_NAME.yml

    # build the function image
    faas-cli up -f $CI_PROJECT_NAME.$CI_COMMIT_REF_NAME.yml

# === Removing OpenFaaS function and the preview environment ===
🗑stop_preview_function:
  stage: 🦄deploy
  tags:
    - openfaas
  only:
    - merge_requests
  when: manual
  environment:
    name: preview/$CI_PROJECT_NAME-$CI_COMMIT_REF_NAME
    action: stop
  script: |
    faas-cli remove -f $CI_PROJECT_NAME.$CI_COMMIT_REF_NAME.yml
```

> Tools

```yaml
.tools: &tools |

  # --- add notes to the merge request ---
  function mr_note() {
    curl -d "body=$1" \
    --request POST --header "PRIVATE-TOKEN: $GL_TOKEN" \
    $GL_URL/api/v4/projects/$CI_MERGE_REQUEST_PROJECT_ID/merge_requests/$CI_MERGE_REQUEST_IID/notes?body=note
  }

before_script:
  - *tools
```

### DEMO 7 (use the express vue template)

> Deploy the 3 functions each time

```yaml
stages:
  - 🚀deploy
  - 🤖deploy

🎉deploy_say-hello:
  stage: 🚀deploy
  tags:
    - openfaas
  environment:
    name: production/say-hello
    url:  $OPENFAAS_URL/function/say-hello
  only:
    - master  
  script: |
    faas-cli up -f ./say-hello.yml

🎉deploy_say-yo:
  stage: 🚀deploy
  tags:
    - openfaas
  environment:
    name: production/say-yo
    url:  $OPENFAAS_URL/function/say-yo
  only:
    - master  
  script: |
    faas-cli up -f ./say-yo.yml

🎉deploy_web-app:
  stage: 🤖deploy
  tags:
    - openfaas
  environment:
    name: production/web-app
    url:  $OPENFAAS_URL/function/web-app
  only:
    - master  
  script: |
    faas-cli up -f ./web-app.yml

```